<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><fmt:message key="clients" /></h6>
	</div>
	
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
				
				<thead>
					<tr>
						<th><fmt:messge key="nom" /></th>
						<th><fmt:messge key="prenom" /></th>
						<th><fmt:messge key="mail" /></th>
						<th><fmt:messge key="adresse" /></th>
					</tr>
				</thead>
			
				<tbody>
					<c:forEach items="${clients}"  var="client">
						<tr>
							<td>${client.nom}</td>
							<td>${client.prenom}</td>
							<td>${client.mail}</td>
							<td>${client.adresse}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

</div>
