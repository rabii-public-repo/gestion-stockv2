<c:url var="home" value="/" />
<c:url var="clientURL" value="/client" />
<c:url var="articleURL" value="/article" />
<c:url var="categorieURL" value="/categorie" />
<c:url var="commandeClientURL" value="/commandeclient"/>
<c:url var="commandeFournisseurURL" value="/commandefournisseur"/>
<c:url var="fournisseurURL" value="/fournisseur"/>
<c:url var="ligneCommandeFournisseurURL" value="/lignecommandefournisseur"/>
<c:url var="ligneCommandeClientURL" value="/lignecommandeclient"/>
<c:url var="ligneVenteURL" value="/lignevente"/>
<c:url var="mouvementStockURL" value="/mouvementstock"/>
<c:url var="utilisateurURL" value="/utilisateur"/>
<c:url var="venteURL" value="/vente"/>
<c:url var="loginURL" value="/login"/>
<c:url var="logoutURL" value="/logout"/>
<c:url var="enregistrerURL" value="/enregistrer"/>
<c:url var="modifierURL" value="/modifier"/>
<c:url var="supprimerURL" value="/supprimer"/>
<c:url var="nouveauURL" value="/nouveau"/>

<!-- Sidebar -->
<!-- Sidebar -->
<ul
	class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
	id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a
		class="sidebar-brand d-flex align-items-center justify-content-center" href="${home}">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>

		<div class="sidebar-brand-text mx-3">
			<fmt:message key="application.name" />
			<sup>2</sup>
		</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item">
		<a class="nav-link" href="${home}"> 
			<i class="fas fa-fw fa-tachometer-alt"></i> 
			<span>
				<fmt:message key="dashbord" />
			</span>
		</a>
	</li>

	<!-- Nav Item - clients -->
	<li class="nav-item">
		<a class="nav-link" href=""> 
			<i class="fas fa-fw fa-table"></i> 
				<span>
					<fmt:message key="articles" />
				</span>
		</a>
	</li>

	<!-- Nav Item - Clients Collapse Menu -->
	<li class="nav-item">
		<a class="nav-link collapsed" href="${clientURL}" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
				aria-controls="collapseTwo">
				<i class="fas fa-fw fa-cog"></i> 
					<span>
						<fmt:message key="clients" />
					</span>
		</a>

		<div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href="${clientURL}">
					<fmt:message key="clients" />
				</a>
				<a class="collapse-item" href="${commandeClientURL}">
					<fmt:message key="commandes.clients" />
				</a>
			</div>
		</div></li>

	<!-- Nav Item - Fournisseurs Collapse Menu -->
	<li class="nav-item active"><a class="nav-link" href="#"
		data-toggle="collapse" data-target="#collapsePages"
		aria-expanded="true" aria-controls="collapsePages"> <i
			class="fas fa-fw fa-folder"></i> <span><fmt:message
					key="fournisseurs" /></span>
	</a>
		<div id="collapsePages" class="collapse"
			aria-labelledby="headingPages" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href=""><fmt:message key="fournisseurs" /></a>
				<a class="collapse-item" href=""><fmt:message
						key="commandes.fournisseurs" /></a>
			</div>
		</div></li>

	<!-- Nav Item - Stock -->
	<li class="nav-item"><a class="nav-link" href=""> <i
			class="fas fa-fw fa-table"></i> <span><fmt:message key="stock" /></span>
	</a></li>

	<!-- Nav Item - Vente -->
	<li class="nav-item"><a class="nav-link" href=""> <i
			class="fas fa-fw fa-chart-area"></i> <span><fmt:message
					key="ventes" /></span>
	</a></li>

	<!-- Nav Item - Paramétrage Collapse Menu -->
	<li class="nav-item"><a class="nav-link collapsed" href="#"
		data-toggle="collapse" data-target="#collapseUtilities"
		aria-expanded="true" aria-controls="collapseUtilities"> <i
			class="fas fa-fw fa-wrench"></i> <span><fmt:message
					key="parametrage" /></span>
	</a>
		<div id="collapseUtilities" class="collapse"
			aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
			<div class="bg-white py-2 collapse-inner rounded">
				<a class="collapse-item" href=""><fmt:message key="utilisateurs" /></a>
				<a class="collapse-item" href=""><fmt:message key="categories" /></a>
			</div>
		</div></li>

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
<!-- End of Sidebar -->