<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!-- taglib de jstl core -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!-- taglib pour i18n -->
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!-- taglib pour les formulaires spring -->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page session="true"%>

<%@ page trimDirectiveWhitespaces="true"%>

<%
	String locale = "fr_FR";
%>
<!-- configuration pour i18n -->

<fmt:setLocale value="${locale}" />
<fmt:bundle basename="com.gestion.stock.i18n.applicationresources" />