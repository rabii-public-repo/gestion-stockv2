<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><fmt:message key="clients" /></h6>
	</div>
	
	
	<div class="card-body">
	<div class="table-responsive">
		<div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
				<a href="${clientURL}${nouveauURL}" class="btn btn-light btn-icon-split">
		          	<span class="icon text-gray-600">
		            		<i class="fas fa-arrow-right"></i>
		          	</span>
		          	<span class="text"><fmt:message key="nouveau.client" /></span>
		       </a>
		       &nbsp;&nbsp;
		       <a href="${clientURL}${nouveauURL}" class="btn btn-light btn-icon-split">
		          	<span class="icon text-gray-600">
		            		<i class="fas fa-arrow-right"></i>
		          	</span>
		          	<span class="text">Exporter</span>
		       </a>						
				&nbsp;&nbsp;
		       <a href="${clientURL}${nouveauURL}" class="btn btn-light btn-icon-split">
		          	<span class="icon text-gray-600">
		            		<i class="fas fa-arrow-right"></i>
		          	</span>
		          	<span class="text">Importer</span>
		       </a>						
				
				<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
					
					<thead>
						<tr>
							<th><fmt:message key="nom" /></th>
							<th><fmt:message key="prenom" /></th>
							<th><fmt:message key="mail" /></th>
							<th><fmt:message key="adresse" /></th>
							<th><fmt:message key="actions" /></th>
						</tr>
					</thead>
				
					<tbody>
					<c:forEach items="${clients}" var="client">
 						<tr>
							<td>${client.nom }</td>
							<td>${client.prenom }</td>
							<td>${client.mail }</td>
							<td>${client.adresse }</td>
							<td>
								<a href="${clientURL}${modifierURL}/${client.idClient}" class="btn btn-success btn-circle"> 
									<i class="fas fa-check"></i>
								</a>
								
								<a href="${clientURL}/${client.idClient}" class="btn btn-info btn-circle"> 
									<i class="fas fa-info-circle"></i>
								</a>
								
								<a href="${clientURL}${supprimerURL}/${client.idClient}" class="btn btn-danger btn-circle"> 
									<i class="fas fa-trash"></i>
								</a>
							</td>
							</tr>
					</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

