<div class="card shadow mb-4">
	<div class="card-header py-3">
		<h6 class="m-0 font-weight-bold text-primary"><fmt:message key="enregistrer.client" /></h6>
	</div>
<div class="card-body p-0">
	<!-- Nested Row within Card Body -->
	<div class="row">

		<div class="col-lg-7">
			<div class="p-5">
				<div class="text-center">
				</div>
				<!-- client/modifier -->
				<form:form class="user" method="POST"
					action="${clientURL}${modifierURL}/${client.idClient}" modelAttribute="client">

					<div class="form-group row">
						<div class="col-sm-6 mb-3 mb-sm-0">
							<form:input path="nom" class="form-control form-control-user" 
								placeholder="Nom" />
						</div>
						<div class="col-sm-6">
							<form:input path="prenom" class="form-control form-control-user"
								placeholder="Pr�nom" />
						</div>
					</div>
					<div class="form-group">
						<form:input path="mail" class="form-control form-control-user"
							placeholder="Mail" />
					</div>
					<div class="form-group">
						<form:input path="adresse" class="form-control form-control-user"
							placeholder="Adresse" />
					</div>
					<input type="submit" value="<fmt:message key="enregistrer.client" />"
						 class="btn btn-primary btn-user btn-block" />
				</form:form>
			</div>
		</div>
	</div>

</div>
</div>
