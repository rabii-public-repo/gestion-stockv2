<%@ include file="/WEB-INF/views/taglibs/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">

<head>
<%@ include file="/WEB-INF/views/header/header.jsp"%>
<title><fmt:message key="nouveau.client" /></title>
</head>

<body id="page-top">

	<!-- Page Wrapper -->
	<div id="wrapper">

		<%@ include file="/WEB-INF/views/leftbar/leftbar.jsp"%>

		<!-- Content Wrapper -->
		<div id="content-wrapper" class="d-flex flex-column">

			<!-- Main Content -->
			<div id="content">

				<%@ include file="/WEB-INF/views/topbar/topbar.jsp"%>

				<!-- Begin Page Content -->
				<div class="container-fluid">

					<!-- Page Heading -->
					
					<%@ include file="formulaire-nouveau.jsp"%>
				</div>
				<!-- /.container-fluid -->

			</div>
			<!-- End of Main Content -->

		
			<%@ include file="/WEB-INF/views/footer/footer.jsp"%>
		</div>
		<!-- End of Content Wrapper -->

	</div>
	<!-- End of Page Wrapper -->

	<!-- Scroll to Top Button-->
	<a class="scroll-to-top rounded" href="#page-top"> <i
		class="fas fa-angle-up"></i>
	</a>

	<%@ include file="/WEB-INF/views/logoutmodal/logoutmodal.jsp"%>

	<%@ include file="/WEB-INF/views/script/script.jsp"%>

</body>

</html>
