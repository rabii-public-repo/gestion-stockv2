package com.gestion.stock.mvc.util;

public final class AppUrl {

	public static final String HOME = "/";
	public static final String ARTICLE = "/article";
	public static final String SAVE = "/enregistrer";
	public static final String UPDATE = "/modifier/{id}";
	public static final String DELETE = "/supprimer/{id}";
	public static final String CATEGORIE = "/categorie";
	public static final String CLIENT = "/client";
	public static final String COMMANDE_CLIENT = "/commandeclient";
	public static final String COMMANDE_FOURNISSEUR = "/commandefournisseur";
	public static final String FOURNISSEUR = "/fournisseur";
	public static final String LIGNE_COMMANDE_FOURNISSEUR = "/lignecommandefournisseur";
	public static final String LIGNE_COMMANDE_CLIENT = "/lignecommandeclient";
	public static final String LIGNE_VENTE = "/lignevente";
	public static final String MOUVEMENT_STOCK = "/mouvementstock";
	public static final String UTILISATEUR = "/utilisateur";
	public static final String VENTE = "/vente";
	public static final String LOGIN = "/login";
	public static final String LOGOUT = "/logout";
	public static final String NEW = "/nouveau";

	private AppUrl() {
	}
}
