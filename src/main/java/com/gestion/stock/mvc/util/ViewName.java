package com.gestion.stock.mvc.util;

public final class ViewName {

	public static final String ARTICLE = "article/";
	public static final String FOURNISSEUR = "fournisseur/";
	public static final String LIGNE_COMMANDE_FOURNISSEUR = "lignecommandefournisseur/";
	public static final String CATEGORIE = "categorie/";
	public static final String CLIENT = "client/";
	public static final String COMMANDE_CLIENT = "commandeclient/";
	public static final String LIGNE_COMMANDE_CLIENT = "lignecommandeclient/";
	public static final String COMMANDE_FOURNISSEUR = "commandefournisseur/";
	public static final String VENTE = "vente/";
	public static final String LIGNE_VENTE = "lignevente/";
	public static final String MOUVEMENT_STOCK = "mouvementstock/";
	public static final String UTILISATEUR = "utilisateur/";

	public static final String LIST = "list";
	public static final String NOUVEAU = "nouveau";
	public static final String MODIFIER = "modifier";

	private ViewName() {
	}

}
