package com.gestion.stock.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Categorie {

	@Id
	@SequenceGenerator(name = "categorie_seq", sequenceName = "categorie_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categorie_seq")
	private Long idCategorie;

	private String codeCategorie;
	private String designation;

	@OneToMany(mappedBy = "categorie", fetch = FetchType.LAZY)
	private List<Article> articles;

	public Long getIdCategorie() {
		return idCategorie;
	}

	public void setIdCategorie(Long idCategorie) {
		this.idCategorie = idCategorie;
	}

	public String getCodeCategorie() {
		return codeCategorie;
	}

	public void setCodeCategorie(String codeCategorie) {
		this.codeCategorie = codeCategorie;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public List<Article> getArticles() {
		return articles;
	}

	public void setArticles(List<Article> articles) {
		this.articles = articles;
	}
}
