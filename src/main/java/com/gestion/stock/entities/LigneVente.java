package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class LigneVente {

	@Id
	@SequenceGenerator(name = "ligneVente_seq", sequenceName = "ligneVente_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ligneVente_seq")
	private Long idLigneVente;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idArticle")
	private Article article;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idVente")
	private Vente vente;

	public Long getIdLigneVente() {
		return idLigneVente;
	}

	public void setIdLigneVente(Long idLigneVente) {
		this.idLigneVente = idLigneVente;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Vente getVente() {
		return vente;
	}

	public void setVente(Vente vente) {
		this.vente = vente;
	}

}
