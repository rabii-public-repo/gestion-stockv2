package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class LigneCommandeClient {

	@Id
	@SequenceGenerator(name = "ligneComClient_seq", sequenceName = "ligneComClient_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ligneComClient_seq")
	private Long idLigneComClient;

	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;
	
	@ManyToOne
	@JoinColumn(name = "idCommandeClient")
	private CommandeClient commandeClient;
	
	public Long getIdLigneComClient() {
		return idLigneComClient;
	}
	public void setIdLigneComClient(Long idLigneComClient) {
		this.idLigneComClient = idLigneComClient;
	}
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public CommandeClient getCommandeClient() {
		return commandeClient;
	}
	public void setCommandeClient(CommandeClient commandeClient) {
		this.commandeClient = commandeClient;
	}
}