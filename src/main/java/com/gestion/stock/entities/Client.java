package com.gestion.stock.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity
public class Client {

	@Id
	@SequenceGenerator(name = "client_seq", sequenceName = "client_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "client_seq")
	private Long idClient;
	private String nom;
	private String prenom;
	private String mail;
	private String adresse;

	@OneToMany(mappedBy = "client")
	private List<CommandeClient> commandesClient;

	public Long getIdClient() {
		return idClient;
	}

	public void setIdClient(Long idClient) {
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public List<CommandeClient> getCommandesClient() {
		return commandesClient;
	}

	public void setCommandesClient(List<CommandeClient> commandesClient) {
		this.commandesClient = commandesClient;
	}

}
