package com.gestion.stock.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

@Entity
public class LigneCommandeFournisseur {

	@Id
	@SequenceGenerator(name = "ligneComFournisseur_seq", sequenceName = "ligneComFournisseur_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ligneComFournisseur_seq")
	private Long idLigneComFournisseur;

	@ManyToOne
	@JoinColumn(name = "idArticle")
	private Article article;

	@ManyToOne
	@JoinColumn(name = "idCommandeFournisseur")
	private CommandeFournisseur commandeFournisseur;

	public Long getIdLigneComFournisseur() {
		return idLigneComFournisseur;
	}

	public void setIdLigneComFournisseur(Long idLigneComFournisseur) {
		this.idLigneComFournisseur = idLigneComFournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public CommandeFournisseur getCommandeFournisseur() {
		return commandeFournisseur;
	}

	public void setCommandeFournisseur(CommandeFournisseur commandeFournisseur) {
		this.commandeFournisseur = commandeFournisseur;
	}
}
