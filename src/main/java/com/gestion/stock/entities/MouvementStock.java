package com.gestion.stock.entities;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class MouvementStock {

	public static final Integer ENTREE = 1;
	public static final Integer SORTIE = 2;

	@Id
	@SequenceGenerator(name = "mouvementStck_seq", sequenceName = "mouvementStck_id_seq")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "mouvementStck_seq")
	private Long idMouvementStock;
	private Date dateMouvement;
	private BigDecimal quantite;
	private int typeMouvement;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idArticle")
	private Article article;

	public Long getIdMouvementStock() {
		return idMouvementStock;
	}

	public void setIdMouvementStock(Long idMouvementStock) {
		this.idMouvementStock = idMouvementStock;
	}

	public Date getDateMouvement() {
		return dateMouvement;
	}

	public void setDateMouvement(Date dateMouvement) {
		this.dateMouvement = dateMouvement;
	}

	public BigDecimal getQuantite() {
		return quantite;
	}

	public void setQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public int getTypeMouvement() {
		return typeMouvement;
	}

	public void setTypeMouvement(int typeMouvement) {
		this.typeMouvement = typeMouvement;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

}