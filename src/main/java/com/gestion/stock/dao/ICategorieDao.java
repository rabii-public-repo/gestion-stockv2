package com.gestion.stock.dao;

import com.gestion.stock.entities.Categorie;

public interface ICategorieDao extends IGenericDao<Categorie> {

}
