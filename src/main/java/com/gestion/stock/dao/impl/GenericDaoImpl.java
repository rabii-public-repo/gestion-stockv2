package com.gestion.stock.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.gestion.stock.dao.IGenericDao;
import com.gestion.stock.dao.util.DaoUtil;

@SuppressWarnings("unchecked")
@Service
public class GenericDaoImpl<E> implements IGenericDao<E> {

	private Class<E> type;

	@PersistenceContext
	private EntityManager em;

	public GenericDaoImpl() {
		type = (Class<E>) DaoUtil.getTypeArguments(GenericDaoImpl.class, this.getClass()).get(0);

	}

	@Override
	public List<E> selectAll() {
		Query query = em.createQuery("from " + type.getSimpleName());
		return query.getResultList();
	}

	@Override
	public E getOne(Long id) {
		E entity = em.find(type, id);
		return entity;
	}

	@Override
	public E save(E entity) {
		em.persist(entity);
		return entity;
	}

	@Override
	public E update(E entity) {
		em.merge(entity);
		return entity;
	}

	@Override
	public boolean remove(Long id) {
		E entity = (E) em.find(type, id);
		em.remove(entity);
		return entity == null;
	}
}
