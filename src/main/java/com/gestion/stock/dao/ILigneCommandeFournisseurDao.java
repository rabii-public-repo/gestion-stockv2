package com.gestion.stock.dao;

import com.gestion.stock.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurDao extends IGenericDao<LigneCommandeFournisseur> {

}
