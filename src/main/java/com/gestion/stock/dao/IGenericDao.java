package com.gestion.stock.dao;

import java.util.List;

public interface IGenericDao<E> {

	public List<E> selectAll();

	public E getOne(Long id);

	public E save(E entity);

	public E update(E entity);

	public boolean remove(Long id);
	/*
	 * rajouter des methodes selon besoin
	 */
}
