package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.Utilisateur;

public interface IUtilisateurService {

	public List<Utilisateur> selectAll();

	public Utilisateur getOne(Long id);

	public Utilisateur save(Utilisateur entity);

	public Utilisateur update(Utilisateur entity);

	public boolean remove(Long id);
}
