package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.Article;

public interface IArticleService {

	public List<Article> selectAll();

	public Article getOne(Long id);

	public Article save(Article entity);

	public Article update(Article entity);

	public boolean remove(Long id);

}
