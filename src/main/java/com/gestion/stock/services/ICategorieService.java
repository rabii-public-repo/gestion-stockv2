package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.Categorie;

public interface ICategorieService {

	public List<Categorie> selectAll();

	public Categorie getOne(Long id);

	public Categorie save(Categorie entity);

	public Categorie update(Categorie entity);

	public boolean remove(Long id);

}
