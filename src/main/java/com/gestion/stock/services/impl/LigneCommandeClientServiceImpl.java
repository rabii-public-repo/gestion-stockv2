package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneCommandeClientDao;
import com.gestion.stock.entities.LigneCommandeClient;
import com.gestion.stock.services.ILigneCommandeClientService;

@Transactional
public class LigneCommandeClientServiceImpl implements ILigneCommandeClientService {
	private static final Logger log = LoggerFactory.getLogger(LigneCommandeClientServiceImpl.class);

	@Autowired
	private ILigneCommandeClientDao ligneCommCliDao;

	public LigneCommandeClientServiceImpl() {
		log.info("bean LigneCommandeClientServiceImpl cr�e.");

	}
	@Override
	public List<LigneCommandeClient> selectAll() {
		return ligneCommCliDao.selectAll();
	}

	@Override
	public LigneCommandeClient getOne(Long id) {
		return ligneCommCliDao.getOne(id);
	}

	@Override
	public LigneCommandeClient save(LigneCommandeClient entity) {
		return ligneCommCliDao.save(entity);
	}

	@Override
	public LigneCommandeClient update(LigneCommandeClient entity) {
		return ligneCommCliDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return ligneCommCliDao.remove(id);
	}

	public ILigneCommandeClientDao getLigneCommCliDao() {
		return ligneCommCliDao;
	}

	public void setLigneCommCliDao(ILigneCommandeClientDao ligneCommCliDao) {
		this.ligneCommCliDao = ligneCommCliDao;
	}
	
}
