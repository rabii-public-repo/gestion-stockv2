package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IClientDao;
import com.gestion.stock.entities.Client;
import com.gestion.stock.services.IClientService;

@Transactional
public class ClientServiceImpl implements IClientService {

	private static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);
	@Autowired
	private IClientDao clientDao;

	public ClientServiceImpl() {
		log.info("bean ClientServiceImpl cr�e.");
	}
	
	@Override
	public List<Client> selectAll() {
		return clientDao.selectAll();
	}

	@Override
	public Client getOne(Long id) {
		return clientDao.getOne(id);
	}

	@Override
	public Client save(Client entity) {

		return clientDao.save(entity);
	}

	@Override
	public Client update(Client entity) {
		return clientDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return clientDao.remove(id);
	}

	public IClientDao getClientDao() {
		return clientDao;
	}

	public void setClientDao(IClientDao clientDao) {
		this.clientDao = clientDao;
	}
}
