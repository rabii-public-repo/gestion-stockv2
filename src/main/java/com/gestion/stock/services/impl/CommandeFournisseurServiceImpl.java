package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ICommandeFournisseurDao;
import com.gestion.stock.entities.CommandeFournisseur;
import com.gestion.stock.services.ICommandeFournisseurService;

@Transactional
public class CommandeFournisseurServiceImpl implements ICommandeFournisseurService {
	private static final Logger log = LoggerFactory.getLogger(CommandeFournisseurServiceImpl.class);

	@Autowired
	private ICommandeFournisseurDao commFourDao;

	public CommandeFournisseurServiceImpl() {
		log.info("bean CommandeFournisseurServiceImpl cr�e.");

	}
	@Override
	public List<CommandeFournisseur> selectAll() {
		return commFourDao.selectAll();
	}

	@Override
	public CommandeFournisseur getOne(Long id) {
		return commFourDao.getOne(id);
	}

	@Override
	public CommandeFournisseur save(CommandeFournisseur entity) {
		return commFourDao.save(entity);
	}

	@Override
	public CommandeFournisseur update(CommandeFournisseur entity) {
		return commFourDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return commFourDao.remove(id);
	}

	public ICommandeFournisseurDao getCommFourDao() {
		return commFourDao;
	}

	public void setCommFourDao(ICommandeFournisseurDao commFourDao) {
		this.commFourDao = commFourDao;
	}

}
