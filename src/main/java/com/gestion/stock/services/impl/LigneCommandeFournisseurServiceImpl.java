package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneCommandeFournisseurDao;
import com.gestion.stock.entities.LigneCommandeFournisseur;
import com.gestion.stock.services.ILigneCommandeFournisseurService;

@Transactional
public class LigneCommandeFournisseurServiceImpl implements ILigneCommandeFournisseurService {
	private static final Logger log = LoggerFactory.getLogger(LigneCommandeFournisseurServiceImpl.class);

	@Autowired
	private ILigneCommandeFournisseurDao ligneCommFourDao;

	public LigneCommandeFournisseurServiceImpl() {
		log.info("bean LigneCommandeFournisseurServiceImpl cr�e.");
	}
	@Override
	public List<LigneCommandeFournisseur> selectAll() {
		return ligneCommFourDao.selectAll();
	}

	@Override
	public LigneCommandeFournisseur getOne(Long id) {
		return ligneCommFourDao.getOne(id);
	}

	@Override
	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity) {
		return null;
	}

	@Override
	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity) {
		return ligneCommFourDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return ligneCommFourDao.remove(id);
	}

	public ILigneCommandeFournisseurDao getLigneCommFourDao() {
		return ligneCommFourDao;
	}

	public void setLigneCommFourDao(ILigneCommandeFournisseurDao ligneCommFourDao) {
		this.ligneCommFourDao = ligneCommFourDao;
	}

}
