package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IVenteDao;
import com.gestion.stock.entities.Vente;
import com.gestion.stock.services.IVenteService;

@Transactional
public class VenteServiceImpl implements IVenteService {
	private static final Logger log = LoggerFactory.getLogger(VenteServiceImpl.class);

	@Autowired
	private IVenteDao venteDao;

	public VenteServiceImpl() {
		log.info("bean VenteServiceImpl cr�e.");
	}

	@Override
	public List<Vente> selectAll() {
		return venteDao.selectAll();
	}

	@Override
	public Vente getOne(Long id) {
		return venteDao.getOne(id);
	}

	@Override
	public Vente save(Vente entity) {
		return venteDao.save(entity);
	}

	@Override
	public Vente update(Vente entity) {
		return venteDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return venteDao.remove(id);
	}

	public IVenteDao getVenteDao() {
		return venteDao;
	}

	public void setVenteDao(IVenteDao venteDao) {
		this.venteDao = venteDao;
	}

}
