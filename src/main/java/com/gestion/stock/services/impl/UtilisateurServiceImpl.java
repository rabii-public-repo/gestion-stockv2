package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IUtilisateurDao;
import com.gestion.stock.entities.Utilisateur;
import com.gestion.stock.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {
	private static final Logger log = LoggerFactory.getLogger(UtilisateurServiceImpl.class);

	@Autowired
	private IUtilisateurDao utilisateurDao;

	public UtilisateurServiceImpl() {
		log.info("bean UtilisateurServiceImpl cr�e.");
	}

	@Override
	public List<Utilisateur> selectAll() {
		return utilisateurDao.selectAll();
	}

	@Override
	public Utilisateur getOne(Long id) {
		return utilisateurDao.getOne(id);
	}

	@Override
	public Utilisateur save(Utilisateur entity) {
		return utilisateurDao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {
		return utilisateurDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return utilisateurDao.remove(id);
	}

	public IUtilisateurDao getUtilisateurDao() {
		return utilisateurDao;
	}

	public void setUtilisateurDao(IUtilisateurDao utilisateurDao) {
		this.utilisateurDao = utilisateurDao;
	}

}
