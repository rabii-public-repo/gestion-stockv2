package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ICategorieDao;
import com.gestion.stock.entities.Categorie;
import com.gestion.stock.services.ICategorieService;

@Transactional
public class CategorieServiceImpl implements ICategorieService {
	private static final Logger log = LoggerFactory.getLogger(CategorieServiceImpl.class);
	@Autowired
	private ICategorieDao catDao;

	public CategorieServiceImpl() {
		log.info("bean CategorieServiceImpl cr�e.");
	}

	@Override
	public List<Categorie> selectAll() {
		return catDao.selectAll();
	}

	@Override
	public Categorie getOne(Long id) {
		return catDao.getOne(id);
	}

	@Override
	public Categorie save(Categorie entity) {
		return catDao.save(entity);
	}

	@Override
	public Categorie update(Categorie entity) {
		return catDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return catDao.remove(id);
	}

	public ICategorieDao getCatDao() {
		return catDao;
	}

	public void setCatDao(ICategorieDao catDao) {
		this.catDao = catDao;
	}
}
