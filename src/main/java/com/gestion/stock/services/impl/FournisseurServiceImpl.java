package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IFournisseurDao;
import com.gestion.stock.entities.Fournisseur;
import com.gestion.stock.services.IFournisseurService;

@Transactional
public class FournisseurServiceImpl implements IFournisseurService {
	private static final Logger log = LoggerFactory.getLogger(ArticleServiceImpl.class);

	@Autowired
	private IFournisseurDao fourDao;

	public FournisseurServiceImpl() {
		log.info("bean FournisseurServiceImpl cr�e.");
	}
	@Override
	public List<Fournisseur> selectAll() {
		return fourDao.selectAll();
	}

	@Override
	public Fournisseur getOne(Long id) {
		return fourDao.getOne(id);
	}

	@Override
	public Fournisseur save(Fournisseur entity) {
		return fourDao.save(entity);
	}

	@Override
	public Fournisseur update(Fournisseur entity) {
		return fourDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return fourDao.remove(id);
	}

	public IFournisseurDao getFourDao() {
		return fourDao;
	}

	public void setFourDao(IFournisseurDao fourDao) {
		this.fourDao = fourDao;
	}

}
