package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.controllers.ArticleController;
import com.gestion.stock.dao.IArticleDao;
import com.gestion.stock.entities.Article;
import com.gestion.stock.services.IArticleService;

@Transactional
public class ArticleServiceImpl implements IArticleService {
	private static final Logger log = LoggerFactory.getLogger(ArticleServiceImpl.class);

	@Autowired
	private IArticleDao articleDao;

	public ArticleServiceImpl() {
		log.info("bean ArticleServiceImpl cr�e.");
	}

	@Override
	public List<Article> selectAll() {
		return articleDao.selectAll();
	}

	@Override
	public Article getOne(Long id) {
		return articleDao.getOne(id);
	}

	@Override
	public Article save(Article entity) {
		return articleDao.save(entity);
	}

	@Override
	public Article update(Article entity) {
		return articleDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return articleDao.remove(id);
	}

	public IArticleDao getArticleDao() {
		return articleDao;
	}

	public void setArticleDao(IArticleDao articleDao) {
		this.articleDao = articleDao;
	}

}
