package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IMouvementStockDao;
import com.gestion.stock.entities.MouvementStock;
import com.gestion.stock.services.IMouvementStockService;

@Transactional
public class MouvementStockServiceImpl implements IMouvementStockService {
	private static final Logger log = LoggerFactory.getLogger(MouvementStockServiceImpl.class);

	@Autowired
	private IMouvementStockDao mouvementStockDao;

	public MouvementStockServiceImpl() {
		log.info("bean MouvementStockServiceImpl cr�e.");

	}
	@Override
	public List<MouvementStock> selectAll() {
		return mouvementStockDao.selectAll();
	}

	@Override
	public MouvementStock getOne(Long id) {
		return mouvementStockDao.getOne(id);
	}

	@Override
	public MouvementStock save(MouvementStock entity) {
		return mouvementStockDao.save(entity);
	}

	@Override
	public MouvementStock update(MouvementStock entity) {
		return mouvementStockDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return mouvementStockDao.remove(id);
	}

	public IMouvementStockDao getMouvementStockDao() {
		return mouvementStockDao;
	}

	public void setMouvementStockDao(IMouvementStockDao mouvementStockDao) {
		this.mouvementStockDao = mouvementStockDao;
	}
}
