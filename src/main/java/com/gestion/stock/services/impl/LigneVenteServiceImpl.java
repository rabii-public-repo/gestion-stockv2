package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneVenteDao;
import com.gestion.stock.entities.LigneVente;
import com.gestion.stock.services.ILigneVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService {
	private static final Logger log = LoggerFactory.getLogger(LigneVenteServiceImpl.class);

	@Autowired
	private ILigneVenteDao ligneVenteDao;

	public LigneVenteServiceImpl() {
		log.info("bean LigneVenteServiceImpl cr�e.");
	}

	@Override
	public List<LigneVente> selectAll() {
		return ligneVenteDao.selectAll();
	}

	@Override
	public LigneVente getOne(Long id) {
		return ligneVenteDao.getOne(id);
	}

	@Override
	public LigneVente save(LigneVente entity) {
		return ligneVenteDao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {
		return ligneVenteDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return ligneVenteDao.remove(id);
	}

	public ILigneVenteDao getLigneVenteDao() {
		return ligneVenteDao;
	}

	public void setLigneVenteDao(ILigneVenteDao ligneVenteDao) {
		this.ligneVenteDao = ligneVenteDao;
	}

}
