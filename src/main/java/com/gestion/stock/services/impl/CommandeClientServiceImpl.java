package com.gestion.stock.services.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ICommandeClientDao;
import com.gestion.stock.entities.CommandeClient;
import com.gestion.stock.services.ICommandeClientService;

@Transactional
public class CommandeClientServiceImpl implements ICommandeClientService {

	private static final Logger log = LoggerFactory.getLogger(CommandeClientServiceImpl.class);

	@Autowired
	private ICommandeClientDao commandeClientDao;

	public CommandeClientServiceImpl() {
		log.info("bean CommandeClientServiceImpl cr�e.");
	}
	@Override
	public List<CommandeClient> selectAll() {
		return commandeClientDao.selectAll();
	}

	@Override
	public CommandeClient getOne(Long id) {
		return commandeClientDao.getOne(id);
	}

	@Override
	public CommandeClient save(CommandeClient entity) {
		return commandeClientDao.save(entity);
	}

	@Override
	public CommandeClient update(CommandeClient entity) {
		return commandeClientDao.update(entity);
	}

	@Override
	public boolean remove(Long id) {
		return commandeClientDao.remove(id);
	}

	public ICommandeClientDao getCommandeClientDao() {
		return commandeClientDao;
	}

	public void setCommandeClientDao(ICommandeClientDao commandeClientDao) {
		this.commandeClientDao = commandeClientDao;
	}

}
