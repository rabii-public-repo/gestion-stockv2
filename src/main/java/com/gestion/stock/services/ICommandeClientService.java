package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.CommandeClient;

public interface ICommandeClientService {

	public List<CommandeClient> selectAll();

	public CommandeClient getOne(Long id);

	public CommandeClient save(CommandeClient entity);

	public CommandeClient update(CommandeClient entity);

	public boolean remove(Long id);
}
