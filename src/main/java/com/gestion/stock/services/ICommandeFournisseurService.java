package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.CommandeFournisseur;

public interface ICommandeFournisseurService {

	public List<CommandeFournisseur> selectAll();

	public CommandeFournisseur getOne(Long id);

	public CommandeFournisseur save(CommandeFournisseur entity);

	public CommandeFournisseur update(CommandeFournisseur entity);

	public boolean remove(Long id);
}
