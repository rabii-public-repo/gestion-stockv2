package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.Vente;

public interface IVenteService {

	public List<Vente> selectAll();

	public Vente getOne(Long id);

	public Vente save(Vente entity);

	public Vente update(Vente entity);

	public boolean remove(Long id);

}
