package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.MouvementStock;

public interface IMouvementStockService {

	public List<MouvementStock> selectAll();

	public MouvementStock getOne(Long id);

	public MouvementStock save(MouvementStock entity);

	public MouvementStock update(MouvementStock entity);

	public boolean remove(Long id);
}
