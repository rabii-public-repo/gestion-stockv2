package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.LigneCommandeFournisseur;

public interface ILigneCommandeFournisseurService {

	public List<LigneCommandeFournisseur> selectAll();

	public LigneCommandeFournisseur getOne(Long id);

	public LigneCommandeFournisseur save(LigneCommandeFournisseur entity);

	public LigneCommandeFournisseur update(LigneCommandeFournisseur entity);

	public boolean remove(Long id);

}
