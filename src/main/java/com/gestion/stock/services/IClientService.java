package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.Client;

public interface IClientService {

	public List<Client> selectAll();

	public Client getOne(Long id);

	public Client save(Client entity);

	public Client update(Client entity);

	public boolean remove(Long id);
}
