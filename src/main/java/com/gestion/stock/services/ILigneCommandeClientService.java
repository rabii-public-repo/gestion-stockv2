package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.LigneCommandeClient;

public interface ILigneCommandeClientService {

	public List<LigneCommandeClient> selectAll();

	public LigneCommandeClient getOne(Long id);

	public LigneCommandeClient save(LigneCommandeClient entity);

	public LigneCommandeClient update(LigneCommandeClient entity);

	public boolean remove(Long id);
}
