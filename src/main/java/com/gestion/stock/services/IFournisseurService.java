package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.Fournisseur;

public interface IFournisseurService {

	public List<Fournisseur> selectAll();

	public Fournisseur getOne(Long id);

	public Fournisseur save(Fournisseur entity);

	public Fournisseur update(Fournisseur entity);

	public boolean remove(Long id);

}
