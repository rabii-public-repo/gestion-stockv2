package com.gestion.stock.services;

import java.util.List;

import com.gestion.stock.entities.LigneVente;

public interface ILigneVenteService {

	public List<LigneVente> selectAll();

	public LigneVente getOne(Long id);

	public LigneVente save(LigneVente entity);

	public LigneVente update(LigneVente entity);

	public boolean remove(Long id);

}
