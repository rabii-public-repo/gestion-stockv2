package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.Vente;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.IVenteService;

@Controller

@RequestMapping(value = AppUrl.VENTE)
public class VenteController {

	private static final Logger log = LoggerFactory.getLogger(VenteController.class);

	private final IVenteService venteServ;

	@Autowired
	public VenteController(IVenteService venteServ) {
		this.venteServ = venteServ;
	}

	@RequestMapping
	public String vente(Model model) {
		List<Vente> ventes = venteServ.selectAll();
		model.addAttribute("ventes", ventes);
		return ViewName.VENTE + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("vente", new Vente());
		return ViewName.VENTE + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		Vente vente = venteServ.getOne(id);
		model.addAttribute("vente", vente);
		return ViewName.VENTE + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, Vente vente) {
		vente = venteServ.save(vente);
		model.addAttribute("vente", vente);
		return "redirect:" + AppUrl.VENTE;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("vente", new Vente());
		} else {
			Vente vente = venteServ.getOne(id);
			if (vente == null) {
				model.addAttribute("vente", new Vente());
			} else {
				vente = venteServ.update(vente);
				model.addAttribute("vente", vente);
			}
		}
		return "redirect:" + AppUrl.VENTE;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("vente", new Vente());
		} else {
			Vente vente = venteServ.getOne(id);
			venteServ.remove(id);
			model.addAttribute("vente", vente);
		}
		return "redirect:" + AppUrl.VENTE;
	}
}
