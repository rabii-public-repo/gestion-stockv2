package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.CommandeFournisseur;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.ICommandeFournisseurService;

@Controller

@RequestMapping(value = AppUrl.COMMANDE_FOURNISSEUR)
public class CommandeFournisseurController {

	private static final Logger log = LoggerFactory.getLogger(CommandeFournisseurController.class);

	private final ICommandeFournisseurService commFourServ;

	@Autowired
	public CommandeFournisseurController(ICommandeFournisseurService commFourServ) {
		this.commFourServ = commFourServ;
	}

	@RequestMapping
	public String commandeFournisseur(Model model) {
		List<CommandeFournisseur> commandeFournisseurs = commFourServ.selectAll();
		model.addAttribute("commandeFournisseurs", commandeFournisseurs);
		return ViewName.COMMANDE_FOURNISSEUR + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("commandeFournisseur", new CommandeFournisseur());
		return ViewName.COMMANDE_FOURNISSEUR + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		CommandeFournisseur commandeFournisseur = commFourServ.getOne(id);
		model.addAttribute("commandeFournisseur", commandeFournisseur);
		return ViewName.COMMANDE_FOURNISSEUR + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, CommandeFournisseur commandeFournisseur) {
		commandeFournisseur = commFourServ.save(commandeFournisseur);
		model.addAttribute("commandeFournisseur", commandeFournisseur);
		return "redirect:" + AppUrl.COMMANDE_FOURNISSEUR;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("commandeFournisseur", new CommandeFournisseur());
		} else {
			CommandeFournisseur commandeFournisseur = commFourServ.getOne(id);
			if (commandeFournisseur == null) {
				model.addAttribute("commandeFournisseur", new CommandeFournisseur());
			} else {
				commandeFournisseur = commFourServ.update(commandeFournisseur);
				model.addAttribute("commandeFournisseur", commandeFournisseur);
			}
		}
		return "redirect:" + AppUrl.COMMANDE_FOURNISSEUR;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("commandeFournisseur", new CommandeFournisseur());
		} else {
			CommandeFournisseur commandeFournisseur = commFourServ.getOne(id);
			commFourServ.remove(id);
			model.addAttribute("commandeFournisseur", commandeFournisseur);
		}
		return "redirect:" + AppUrl.COMMANDE_FOURNISSEUR;
	}
}
