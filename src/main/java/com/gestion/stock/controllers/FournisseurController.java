package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.Fournisseur;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.IFournisseurService;

@Controller

@RequestMapping(value = AppUrl.FOURNISSEUR)
public class FournisseurController {

	private static final Logger log = LoggerFactory.getLogger(FournisseurController.class);

	private final IFournisseurService fournisseurServ;

	@Autowired
	public FournisseurController(IFournisseurService fournisseurServ) {
		this.fournisseurServ = fournisseurServ;
	}

	@RequestMapping
	public String fournisseur(Model model) {
		List<Fournisseur> fournisseurs = fournisseurServ.selectAll();
		model.addAttribute("fournisseurs", fournisseurs);
		return ViewName.FOURNISSEUR + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("fournisseur", new Fournisseur());
		return ViewName.FOURNISSEUR + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		Fournisseur fournisseur = fournisseurServ.getOne(id);
		model.addAttribute("fournisseur", fournisseur);
		return ViewName.FOURNISSEUR + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, Fournisseur fournisseur) {
		fournisseur = fournisseurServ.save(fournisseur);
		model.addAttribute("fournisseur", fournisseur);
		return "redirect:" + AppUrl.FOURNISSEUR;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("fournisseur", new Fournisseur());
		} else {
			Fournisseur fournisseur = fournisseurServ.getOne(id);
			if (fournisseur == null) {
				model.addAttribute("fournisseur", new Fournisseur());
			} else {
				fournisseur = fournisseurServ.update(fournisseur);
				model.addAttribute("fournisseur", fournisseur);
			}
		}
		return "redirect:" + AppUrl.FOURNISSEUR;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("fournisseur", new Fournisseur());
		} else {
			Fournisseur fournisseur = fournisseurServ.getOne(id);
			fournisseurServ.remove(id);
			model.addAttribute("fournisseur", fournisseur);
		}
		return "redirect:" + AppUrl.FOURNISSEUR;
	}
}
