package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.Article;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.IArticleService;

@Controller
@RequestMapping(value = AppUrl.ARTICLE)
public class ArticleController {

	private static final Logger log = LoggerFactory.getLogger(ArticleController.class);
	private final IArticleService artServ;

	@Autowired
	public ArticleController(IArticleService artServ) {
		this.artServ = artServ;
	}

	@RequestMapping
	public String article(Model model) {
		List<Article> articles = artServ.selectAll();
		model.addAttribute("articles", articles);
		return ViewName.ARTICLE + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("article", new Article());
		return ViewName.ARTICLE + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.GET)
	public String formModifier(Model model,@PathVariable Long id) {
		Article article = artServ.getOne(id);
		model.addAttribute("article", article);
		return ViewName.ARTICLE + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, Article article) {
		article = artServ.save(article);
		model.addAttribute("article", article);
		return "redirect:" + AppUrl.ARTICLE;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("article", new Article());
		} else {
			Article article = artServ.getOne(id);
			if (article == null) {
				model.addAttribute("article", new Article());
			} else {
				article = artServ.update(article);
				model.addAttribute("article", article);
			}
		}
		return "redirect:" + AppUrl.ARTICLE;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("article", new Article());
		} else {
			Article article = artServ.getOne(id);
			artServ.remove(id);
			model.addAttribute("article", article);
		}
		return "redirect:" + AppUrl.ARTICLE;
	}
}
