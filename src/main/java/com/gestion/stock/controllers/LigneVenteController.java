package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.LigneVente;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.ILigneVenteService;

@Controller

@RequestMapping(value = AppUrl.LIGNE_VENTE)
public class LigneVenteController {

	private static final Logger log = LoggerFactory.getLogger(LigneVenteController.class);

	private final ILigneVenteService ligneVenteServ;

	@Autowired
	public LigneVenteController(ILigneVenteService ligneVenteServ) {
		this.ligneVenteServ = ligneVenteServ;
	}

	@RequestMapping
	public String ligneVente(Model model) {
		List<LigneVente> ligneVentes = ligneVenteServ.selectAll();
		model.addAttribute("ligneVentes", ligneVentes);
		return ViewName.LIGNE_VENTE + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("ligneVente", new LigneVente());
		return ViewName.LIGNE_VENTE + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		LigneVente ligneVente = ligneVenteServ.getOne(id);
		model.addAttribute("ligneVente", ligneVente);
		return ViewName.LIGNE_VENTE + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, LigneVente ligneVente) {
		ligneVente = ligneVenteServ.save(ligneVente);
		model.addAttribute("ligneVente", ligneVente);
		return "redirect:" + AppUrl.LIGNE_VENTE;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("ligneVente", new LigneVente());
		} else {
			LigneVente ligneVente = ligneVenteServ.getOne(id);
			if (ligneVente == null) {
				model.addAttribute("ligneVente", new LigneVente());
			} else {
				ligneVente = ligneVenteServ.update(ligneVente);
				model.addAttribute("ligneVente", ligneVente);
			}
		}
		return "redirect:" + AppUrl.LIGNE_VENTE;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("ligneVente", new LigneVente());
		} else {
			LigneVente ligneVente = ligneVenteServ.getOne(id);
			ligneVenteServ.remove(id);
			model.addAttribute("ligneVente", ligneVente);
		}
		return "redirect:" + AppUrl.LIGNE_VENTE;
	}
}
