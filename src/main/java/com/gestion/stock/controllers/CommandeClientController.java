package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.CommandeClient;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.ICommandeClientService;

@Controller

@RequestMapping(value = AppUrl.COMMANDE_CLIENT)
public class CommandeClientController {

	private static final Logger log = LoggerFactory.getLogger(CommandeClientController.class);

	private final ICommandeClientService commCliServ;

	@Autowired
	public CommandeClientController(ICommandeClientService commCliServ) {
		this.commCliServ = commCliServ;
	}

	@RequestMapping
	public String commandeClients(Model model) {
		List<CommandeClient> commandeClients = commCliServ.selectAll();
		model.addAttribute("commandeClients", commandeClients);
		return ViewName.COMMANDE_CLIENT + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("commandeClients", new CommandeClient());
		return ViewName.COMMANDE_CLIENT + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		CommandeClient commandeClients = commCliServ.getOne(id);
		model.addAttribute("commandeClients", commandeClients);
		return ViewName.COMMANDE_CLIENT + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, CommandeClient commandeClients) {
		commandeClients = commCliServ.save(commandeClients);
		model.addAttribute("commandeClients", commandeClients);
		return "redirect:" + AppUrl.COMMANDE_CLIENT;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("commandeClients", new CommandeClient());
		} else {
			CommandeClient commandeClients = commCliServ.getOne(id);
			if (commandeClients == null) {
				model.addAttribute("commandeClients", new CommandeClient());
			} else {
				commandeClients = commCliServ.update(commandeClients);
				model.addAttribute("commandeClients", commandeClients);
			}
		}
		return "redirect:" + AppUrl.COMMANDE_CLIENT;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("commandeClients", new CommandeClient());
		} else {
			CommandeClient commandeClients = commCliServ.getOne(id);
			commCliServ.remove(id);
			model.addAttribute("commandeClients", commandeClients);
		}
		return "redirect:" + AppUrl.COMMANDE_CLIENT;
	}
}
