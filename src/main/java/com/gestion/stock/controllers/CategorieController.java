package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.Categorie;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.ICategorieService;

@Controller

@RequestMapping(value = AppUrl.CATEGORIE)
public class CategorieController {

	private static final Logger log = LoggerFactory.getLogger(CategorieController.class);
	private final ICategorieService catServ;

	@Autowired
	public CategorieController(ICategorieService cateServ) {
		this.catServ = cateServ;
	}

	@RequestMapping
	public String categorie(Model model) {
		List<Categorie> categories = catServ.selectAll();
		model.addAttribute("categories", categories);
		return ViewName.CATEGORIE + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("categorie", new Categorie());
		return ViewName.CATEGORIE + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model,@PathVariable Long id) {
		Categorie categorie = catServ.getOne(id);
		model.addAttribute("categorie", categorie);
		return ViewName.CATEGORIE + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, Categorie categorie) {
		categorie = catServ.save(categorie);
		model.addAttribute("categorie", categorie);
		return "redirect:" + AppUrl.CATEGORIE;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("categorie", new Categorie());
		} else {
			Categorie categorie = catServ.getOne(id);
			if (categorie == null) {
				model.addAttribute("categorie", new Categorie());
			} else {
				categorie = catServ.update(categorie);
				model.addAttribute("categorie", categorie);
			}
		}
		return "redirect:" + AppUrl.CATEGORIE;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("categorie", new Categorie());
		} else {
			Categorie categorie = catServ.getOne(id);
			catServ.remove(id);
			model.addAttribute("categorie", categorie);
		}
		return "redirect:" + AppUrl.CATEGORIE;
	}
}
