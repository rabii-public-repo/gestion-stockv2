package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.LigneCommandeClient;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.ILigneCommandeClientService;

@Controller

@RequestMapping(value = AppUrl.LIGNE_COMMANDE_CLIENT)
public class LigneCommandeClientController {

	private static final Logger log = LoggerFactory.getLogger(LigneCommandeClientController.class);

	private final ILigneCommandeClientService ligneCommandeClientServ;

	@Autowired
	public LigneCommandeClientController(ILigneCommandeClientService ligneCommandeClient) {
		this.ligneCommandeClientServ = ligneCommandeClient;
	}

	@RequestMapping
	public String ligneCommandeClients(Model model) {
		List<LigneCommandeClient> ligneCommandeClientss = ligneCommandeClientServ.selectAll();
		model.addAttribute("ligneCommandeClientss", ligneCommandeClientss);
		return ViewName.LIGNE_COMMANDE_CLIENT + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("ligneCommandeClients", new LigneCommandeClient());
		return ViewName.LIGNE_COMMANDE_CLIENT + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		LigneCommandeClient ligneCommandeClients = ligneCommandeClientServ.getOne(id);
		model.addAttribute("ligneCommandeClients", ligneCommandeClients);
		return ViewName.LIGNE_COMMANDE_CLIENT + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, LigneCommandeClient ligneCommandeClients) {
		ligneCommandeClients = ligneCommandeClientServ.save(ligneCommandeClients);
		model.addAttribute("ligneCommandeClients", ligneCommandeClients);
		return "redirect:" + AppUrl.LIGNE_COMMANDE_CLIENT;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("ligneCommandeClients", new LigneCommandeClient());
		} else {
			LigneCommandeClient ligneCommandeClients = ligneCommandeClientServ.getOne(id);
			if (ligneCommandeClients == null) {
				model.addAttribute("ligneCommandeClients", new LigneCommandeClient());
			} else {
				ligneCommandeClients = ligneCommandeClientServ.update(ligneCommandeClients);
				model.addAttribute("ligneCommandeClients", ligneCommandeClients);
			}
		}
		return "redirect:" + AppUrl.LIGNE_COMMANDE_CLIENT;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("ligneCommandeClients", new LigneCommandeClient());
		} else {
			LigneCommandeClient ligneCommandeClients = ligneCommandeClientServ.getOne(id);
			ligneCommandeClientServ.remove(id);
			model.addAttribute("ligneCommandeClients", ligneCommandeClients);
		}
		return "redirect:" + AppUrl.LIGNE_COMMANDE_CLIENT;
	}
}
