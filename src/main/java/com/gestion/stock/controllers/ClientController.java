package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.Client;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.IClientService;

@Controller
@RequestMapping(value = AppUrl.CLIENT)
public class ClientController {

	private static final Logger log = LoggerFactory.getLogger(ClientController.class);

	private final IClientService clientServ;

	@Autowired
	public ClientController(IClientService clientServ) {
		this.clientServ = clientServ;
	}

	@RequestMapping
	public String client(Model model) {
		List<Client> clients = clientServ.selectAll();
		model.addAttribute("clients", clients);
		return ViewName.CLIENT + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("client", new Client());
		return ViewName.CLIENT + ViewName.NOUVEAU;
	}

	// http://localhost:8080/client/modifier/{id}
	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		Client client = clientServ.getOne(id);
		model.addAttribute("client", client);
		return ViewName.CLIENT + ViewName.MODIFIER;
	}

	// http://localhost:8080/client/enregistrer
	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, Client client) {
		client = clientServ.save(client);
		return "redirect:" + AppUrl.CLIENT;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(@PathVariable Long id, Client client) {
		clientServ.update(client);
		return "redirect:" + AppUrl.CLIENT;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(@PathVariable Long id) {
		clientServ.remove(id);
		return "redirect:" + AppUrl.CLIENT;
	}
}
