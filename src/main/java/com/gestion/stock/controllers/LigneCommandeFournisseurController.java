package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.LigneCommandeFournisseur;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.ILigneCommandeFournisseurService;

@Controller

@RequestMapping(value = AppUrl.LIGNE_COMMANDE_FOURNISSEUR)
public class LigneCommandeFournisseurController {

	private static final Logger log = LoggerFactory.getLogger(LigneCommandeFournisseurController.class);

	private final ILigneCommandeFournisseurService ligneCommFournisseurServ;

	@Autowired
	public LigneCommandeFournisseurController(ILigneCommandeFournisseurService ligneCommFournisseurServ) {
		this.ligneCommFournisseurServ = ligneCommFournisseurServ;
	}

	@RequestMapping
	public String ligneCommandeFournisseur(Model model) {
		List<LigneCommandeFournisseur> ligneCommandeFournisseurs = ligneCommFournisseurServ.selectAll();
		model.addAttribute("ligneCommandeFournisseurs", ligneCommandeFournisseurs);
		return ViewName.LIGNE_COMMANDE_FOURNISSEUR + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("ligneCommandeFournisseur", new LigneCommandeFournisseur());
		return ViewName.LIGNE_COMMANDE_FOURNISSEUR + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		LigneCommandeFournisseur ligneCommandeFournisseur = ligneCommFournisseurServ.getOne(id);
		model.addAttribute("ligneCommandeFournisseur", ligneCommandeFournisseur);
		return ViewName.LIGNE_COMMANDE_FOURNISSEUR + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, LigneCommandeFournisseur ligneCommandeFournisseur) {
		ligneCommandeFournisseur = ligneCommFournisseurServ.save(ligneCommandeFournisseur);
		model.addAttribute("ligneCommandeFournisseur", ligneCommandeFournisseur);
		return "redirect:" + AppUrl.LIGNE_COMMANDE_FOURNISSEUR;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("ligneCommandeFournisseur", new LigneCommandeFournisseur());
		} else {
			LigneCommandeFournisseur ligneCommandeFournisseur = ligneCommFournisseurServ.getOne(id);
			if (ligneCommandeFournisseur == null) {
				model.addAttribute("ligneCommandeFournisseur", new LigneCommandeFournisseur());
			} else {
				ligneCommandeFournisseur = ligneCommFournisseurServ.update(ligneCommandeFournisseur);
				model.addAttribute("ligneCommandeFournisseur", ligneCommandeFournisseur);
			}
		}
		return "redirect:" + AppUrl.LIGNE_COMMANDE_FOURNISSEUR;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("ligneCommandeFournisseur", new LigneCommandeFournisseur());
		} else {
			LigneCommandeFournisseur ligneCommandeFournisseur = ligneCommFournisseurServ.getOne(id);
			ligneCommFournisseurServ.remove(id);
			model.addAttribute("ligneCommandeFournisseur", ligneCommandeFournisseur);
		}
		return "redirect:" + AppUrl.LIGNE_COMMANDE_FOURNISSEUR;
	}
}
