package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.Utilisateur;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.IUtilisateurService;

@Controller

@RequestMapping(value = AppUrl.UTILISATEUR)
public class UtilisateurController {

	private static final Logger log = LoggerFactory.getLogger(UtilisateurController.class);

	private final IUtilisateurService utilisateurServ;

	@Autowired
	public UtilisateurController(IUtilisateurService utilisateurServ) {
		this.utilisateurServ = utilisateurServ;
	}

	@RequestMapping
	public String utilisateur(Model model) {
		List<Utilisateur> utilisateurs = utilisateurServ.selectAll();
		model.addAttribute("utilisateurs", utilisateurs);
		return ViewName.UTILISATEUR + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("utilisateur", new Utilisateur());
		return ViewName.UTILISATEUR + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		Utilisateur utilisateur = utilisateurServ.getOne(id);
		model.addAttribute("utilisateur", utilisateur);
		return ViewName.UTILISATEUR + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, Utilisateur utilisateur) {
		utilisateur = utilisateurServ.save(utilisateur);
		model.addAttribute("utilisateur", utilisateur);
		return "redirect:" + AppUrl.UTILISATEUR;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("utilisateur", new Utilisateur());
		} else {
			Utilisateur utilisateur = utilisateurServ.getOne(id);
			if (utilisateur == null) {
				model.addAttribute("utilisateur", new Utilisateur());
			} else {
				utilisateur = utilisateurServ.update(utilisateur);
				model.addAttribute("utilisateur", utilisateur);
			}
		}
		return "redirect:" + AppUrl.UTILISATEUR;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("utilisateur", new Utilisateur());
		} else {
			Utilisateur utilisateur = utilisateurServ.getOne(id);
			utilisateurServ.remove(id);
			model.addAttribute("utilisateur", utilisateur);
		}
		return "redirect:" + AppUrl.UTILISATEUR;
	}
}
