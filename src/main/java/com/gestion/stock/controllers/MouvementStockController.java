package com.gestion.stock.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.gestion.stock.entities.MouvementStock;
import com.gestion.stock.mvc.util.AppUrl;
import com.gestion.stock.mvc.util.ViewName;
import com.gestion.stock.services.IMouvementStockService;

@Controller

@RequestMapping(value = AppUrl.MOUVEMENT_STOCK)
public class MouvementStockController {

	private static final Logger log = LoggerFactory.getLogger(MouvementStockController.class);

	private final IMouvementStockService mouvementStockServ;

	@Autowired
	public MouvementStockController(IMouvementStockService mouvementStockServ) {
		this.mouvementStockServ = mouvementStockServ;
	}

	@RequestMapping
	public String mouvementStock(Model model) {
		List<MouvementStock> mouvementStocks = mouvementStockServ.selectAll();
		model.addAttribute("mouvementStocks", mouvementStocks);
		return ViewName.MOUVEMENT_STOCK + ViewName.LIST;
	}

	@RequestMapping(value = AppUrl.NEW)
	public String formEnregistrer(Model model) {
		model.addAttribute("mouvementStock", new MouvementStock());
		return ViewName.MOUVEMENT_STOCK + ViewName.NOUVEAU;
	}

	@RequestMapping(value = AppUrl.UPDATE)
	public String formModifier(Model model, @PathVariable Long id) {
		MouvementStock mouvementStock = mouvementStockServ.getOne(id);
		model.addAttribute("mouvementStock", mouvementStock);
		return ViewName.MOUVEMENT_STOCK + ViewName.MODIFIER;
	}

	@RequestMapping(value = AppUrl.SAVE, method = RequestMethod.POST)
	public String enregistrer(Model model, MouvementStock mouvementStock) {
		mouvementStock = mouvementStockServ.save(mouvementStock);
		model.addAttribute("mouvementStock", mouvementStock);
		return "redirect:" + AppUrl.MOUVEMENT_STOCK;
	}

	@RequestMapping(value = AppUrl.UPDATE, method = RequestMethod.POST)
	public String modifier(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("mouvementStock", new MouvementStock());
		} else {
			MouvementStock mouvementStock = mouvementStockServ.getOne(id);
			if (mouvementStock == null) {
				model.addAttribute("mouvementStock", new MouvementStock());
			} else {
				mouvementStock = mouvementStockServ.update(mouvementStock);
				model.addAttribute("mouvementStock", mouvementStock);
			}
		}
		return "redirect:" + AppUrl.MOUVEMENT_STOCK;
	}

	@RequestMapping(value = AppUrl.DELETE, method = RequestMethod.GET)
	public String supprimer(Model model, @PathVariable Long id) {
		if (id == null) {
			model.addAttribute("mouvementStock", new MouvementStock());
		} else {
			MouvementStock mouvementStock = mouvementStockServ.getOne(id);
			mouvementStockServ.remove(id);
			model.addAttribute("mouvementStock", mouvementStock);
		}
		return "redirect:" + AppUrl.MOUVEMENT_STOCK;
	}
}
